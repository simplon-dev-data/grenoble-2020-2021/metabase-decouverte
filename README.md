# Metabase : Découverte

> Découverte du logiciel d'analyse de données Metabase

## Objectifs

- Découvrir un logiciel professionnel d'analyse de données
- Découvrir l'univers du domaine _Business Intelligence_ (_BI_)
- Créer des tableaux de bords de visualisations de données

## Modalités

Cette activité est **à réaliser individuellement**, mais en **réfléchissant en groupe** :

- Chaque apprenant doit compléter ce document sur sa propre branche nommée `prenom-nom`
- Penser à cocher les cases à chaque étape réalisée
- Compléter chaque emplacement marqué `[A COMPLETER]`

## Partie 1 : installation

Avant de pouvoir commencer à explorer Metabase, il est nécessaire l'environnement
d'exécution Docker :

- [ ] Installer Docker :
```bash
sudo apt-get update
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"

sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io
```

- [ ] Vérifier que Docker fonctionne :
```bash
docker --version
```

- [ ] Installer Docker Compose :
```bash
sudo curl -L \
    "https://github.com/docker/compose/releases/download/1.27.4/docker-compose-$(uname -s)-$(uname -m)" \
    -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

sudo curl -L https://raw.githubusercontent.com/docker/compose/1.27.4/contrib/completion/bash/docker-compose \
    -o /etc/bash_completion.d/docker-compose
```

- [ ] Vérifier que Docker Compose fonctionne :
```bash
docker-compose --version
```

- [ ] Lancer Metabase :

```bash
sudo docker-compose up -d
sudo docker-compose ps
```

- [ ] Vérifier que Metabase est lancé en ouvrant un navigateur à l'adresse [http://localhost:3000](http://localhost:3000) (cela peut prendre quelques minutes)

## Partie 2 : charger des données propres

Afin d'analyser des données avec Metabase, il est généralement conseillé d'utiliser
des données propres (données brutes préalablement transformées et nettoyées si nécessaires).

Metabase est utilisé en se connectant à des bases de données et data-warehouses.
Pour cette activité, nous allons reprendre le jeu de données des
[Points d'apport volontaires](https://data.metropolegrenoble.fr/ckan/dataset/points-d-apports-volontaire)
de la Métropole de Grenoble, initialement utilisé lors de la prairie.

- [ ] Installer la base de données SQLite dans le dossier ".docker/metabase"

**Si vous avez encore à disposition votre base de données SQLite de la prairie** :
vous pouvez la réutiliser en copiant votre base dans le dossier ".docker/metabase" :

```bash
cp chemin/vers/ma/base.db .docker/metabase/data.db
```

**Si vous n'avez plus à disposition votre base de données SQLite de la prairie** :
voici quelques commandes pour regénérer la base SQLite au bon endroit :

```bash
curl -o metro_pav.csv "https://entrepot.metropolegrenoble.fr/opendata/200040715-MET/DECHETS/csv/PAV_EPSG4326.csv"

sqlite3 .docker/metabase/data.db
> .mode csv
> .separator ,
> .import metro_pav.csv metro_pav
```

- [ ] Une fois la base SQLite placée au bon endroit, l'ajouter dans Metabase :

    - Cliquer sur "Settings" / "Admin" (en haut à droite)
    - Cliquer sur "Databases" (menu du haut)
    - Cliquer sur "Add database"
    - Remplir les champs :
        - Database type : "SQLite"
        - Name : "Metropole - Points d'apport volontaire"
        - Filename : "/metabase-data/data.db"
    - Cliquer sur "Data Model" (menu du haut)
    - Vérifier que les champs de la table "Metro Pav" sont dans les bons types de données
    - Cliquer sur "Settings" / "Exit admin" (en haut à droite)

## Partie 3 : exploration de données

Metabase permet d'explorer les données des bases connectées de façon interactive.

Une approche simple :

- Cliquer sur "Ask a question" (en haut à gauche)
- Sélectionner "Simple question"
- Sélectionner la base de données "Metropole - Points d'apport volontaire"
- Sélectionner la table "Metro Pav"
- Jouer avec les options d'exploration : filtrage, aggregations, etc.
- Une fois les données souhaitées extraites, cliquer sur "Visualization" et trouver la visualisation adaptée
- Sauvegarder le travail en cliquant sur "Save" (en haut à droite)
- Ajouter la visualisation à un tableau de bord lorsque demandé

### Exercice

La manipulation de données avec Metabase ressemble beaucoup à l'utilisation d'un tableur,
que ce soit dans les vues filtrées et les tables pivots ! Un avantage des outils comme
Metabase est la capacité à créer des tableaux de bords interactifs et dynamiques.

Tenter de reproduire les explorations de données effectuées initialement avec Google Sheets,
permettant de répondre aux questions initialement posées.

### Observations

[A REMPLIR]
